<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="fr" lang="fr">
  <head>
    <title>Travail collaboratif</title>
    <link rel="stylesheet" href="../styles/librasso.css" type="text/css" />
  </head>

  <body epub:type="bodymatter chapter">
    <h2 id="collaboratif" class="titre-chapitre">Travail collaboratif</h2>

    <p>Le caractère dynamique des sites Web, la simplification technique et les capacités collaboratives (nous avons sciemment évité le terme générique <i lang="en" xml:lang="en">marketing</i> et fourre-tout « Web <abbr>2.0</abbr> » pour désigner globalement ces évolutions) offrent aux associations une possibilité jusque-là inégalée de produire de manière collaborative des documents avec les membres de l'association, d'interagir avec les adhérents ou même de coproduire des contenus avec les partenaires de l'association. Disponibles par l'intermédiaire d'un navigateur, les outils sont accessibles par le Web et les documents peuvent donc être modifiés au moment qui convient le mieux au contributeur, et ce avec un suivi automatisé des modifications. La peur de modifications « sauvages », qui peut être identifiée comme l'un des freins à utiliser des outils collaboratifs, est à atténuer par la manière dont peuvent être paramétrés les logiciels : les droits d'accès ou la nécessité de s'identifier peuvent par exemple rassurer pour mettre en place une dynamique d'interaction par l'intermédiaire des outils libres de collaboration.</p>

    <section epub:type="bodymatter subchapter">
      <h3 id="wiki">Wiki</h3>

      <p>L'un des outils caractérisant sans doute le mieux le Web collaboratif est le Wiki : une fois installé, il ouvre de nombreuses perspectives de collaboration entre membres. Cependant, ce n'est pas parce que vous serez parvenu à installer ce type de service que l'ensemble des acteurs de l'association vont se ruer pour participer.</p>
      <p>En effet, même si la pratique du Wiki tend à se démocratiser, deux barrières peuvent encore entraver le développement de cette pratique au sein de votre association. La syntaxe propre au Wiki est un premier obstacle : même si elle paraît minimaliste et évidente à certains, il convient de prévoir un accompagnement des personnes souhaitant s'initier à la rédaction. On notera au passage qu'avec une telle syntaxe, on rejoint la logique d'une suite éditoriale, le Wiki vous contraindra d'abord à vous concentrer sur le fond puisque vous ne pourrez pas élaborer une mise en forme avancée.</p>
      <p>Le second obstacle est moins concret et sans doute moins facile à dépasser : il s'agit de l'inquiétude liée au devenir des écrits produits. Est-ce que tout le monde pourra lire ce que j'ai écrit ? Ai-je le droit d'effacer un contenu produit par quelqu'un d'autre ? Puis-je utiliser les écrits d'une page Wiki ? Peut-on utiliser mes écrits ?, <abbr>etc.</abbr> Là encore, un accompagnement doit permettre de présenter les différents moyens de répondre à ces questions : politique d'identification en fonction des contenus, licence associée au Wiki mis en place, historique des modifications, <abbr>etc.</abbr></p>

      <dl id="mediawiki">
        <dt class="logiciel">Mediawiki</dt>
        <dd class="type">Moteur de Wiki</dd>
        <dd>
          <ul>
            <li class="apprent">Apprentissage : <img src="../images/level/hirond2.png" alt="moyen" /></li>
            <li class="install">Installation : <img src="../images/level/fleur3.png" alt="difficile" /></li>
          </ul>
        </dd>
      </dl>

      <p>Concernant le choix du moteur libre de Wiki, présentons le plus célèbre d'entre eux : il s'agit de <a href="http://www.mediawiki.org/wiki/MediaWiki/fr">MediaWiki</a> qui a fait ses preuves puisque c'est ce moteur qui fait tourner l'encyclopédie <a href="http://fr.wikipedia.org">Wikipédia</a> !</p>

      <figure>
        <img src="../images/screenshots/wiki-april.png" alt="" class="screen" />
        <figcaption>Le web collaboratif avec un Wiki</figcaption>
      </figure>

      <p>Il s'est développé au fil des années et, s'il n'est sans doute pas le Wiki le plus simple à configurer, sa robustesse n'est plus à démontrer. Si vous souhaitez un Wiki plus modeste, regardez entre autres du côté de <a href="http://www.splitbrain.org/projects/dokuwiki">dokuwiki</a> ou <a href="http://www.pmwiki.org/">pmWiki</a>.</p>
    </section>
    <section epub:type="bodymatter subchapter">
      <h3 id="prod_interactive">Production interactive de documents</h3>

      <p>Plus récemment sont apparus sur le Web les « Pads » : des pages sur lesquelles les internautes invités peuvent contribuer en temps réel sur un seul et même document. La syntaxe est simple pour qui connaît et manipule régulièrement un traitement de texte puisque l'on y retrouve les mêmes fonctions de base. Que ce soit pour finaliser un document ou même prendre des notes à plusieurs en même temps lors d'une réunion téléphonique ou d'une conférence, par exemple, l'intérêt est immédiat.</p>
      <p>Comme pour la plupart des logiciels libres utilisables sur le Web, deux options s'offrent à vous pour utiliser ce type d'outil :</p>

      <p>Vous pouvez vous rendre sur des sites qui hébergent le logiciel déjà installé et créer, en un clic, un nouveau Pad. On peut citer par exemple le <a href="http://framapad.org/">Framapad</a>, proposé par Framasoft, et agrémenté de <i lang="en" xml:lang="en">Mypads</i> qui permet de gérer des dossiers et des groupes d'utilisateurs. On peut de même citer le <a href="http://bn.parinux.org/">bloc-note</a> mis à disposition par Parinux <a epub:type="noteref" href="#note_1" id="ref_1">[1]</a> sur son site. Il vous reste alors à communiquer aux participants l'adresse Internet de la page pour qu'ils vous rejoignent. </p>

      <figure>
        <img src="../images/screenshots/framapad.png" alt="" class="screen" />
        <figcaption>L'écriture collaborative avec Etherpad/Framapad</figcaption>
      </figure>

      <p>Ou bien, si vous possédez les compétences en interne, vous choisirez plutôt d'installer un des logiciels <a href="http://etherpad.org/">Etherpad</a> ou <a href="https://github.com/Pita/etherpad-lite">Etherpad-lite</a> sur vos propres serveurs ou ceux que vous louez chez votre hébergeur.</p>
      <p>Avant de terminer ce paragraphe sur les logiciels permettant de produire des documents de manière collaborative, nous vous invitons à regarder <a href="http://gobby.0x539.de/trac/">Gobby</a>, un autre logiciel prometteur pour la production collaborative de documents. Gobby ne dépend pas d'un serveur Web, et est utilisable directement sur le réseau local de votre association.</p>
    </section>
    <section epub:type="bodymatter subchapter">
      <h3 id="gestion_proj">Gestion de projet</h3>

      <dl id="collabtive">
        <dt class="logiciel">Collabtive</dt>
        <dd class="type">Gestion de projet</dd>
        <dd>
          <ul>
            <li class="apprent">Apprentissage : <img src="../images/level/hirond3.png" alt="difficile" /></li>
            <li class="install">Installation : <img src="../images/level/fleur3.png" alt="difficile" /></li>
          </ul>
        </dd>
      </dl>

      <p><a href="http://collabtive.o-dyn.de/?lang=fr">Collabtive</a> est une application web de gestion de projets collaboratifs. Elle vous permet de planifier des événements, gérer des tâches, partager des documents, envoyer des mails et définir des groupes. L'ergonomie très épurée de l'application est sans conteste son principal atout, mais vous apprécierez également la solidité de ses fonctionnalités de base qui la rendent tout à fait adaptée aux besoins des associations et des réseaux d'acteurs.</p>
      <p>La prise en main est intuitive : vous trouverez rapidement vos repères quel que soit votre niveau en informatique. L'interface graphique apparaît sobre et épurée, ainsi le regard se pose directement sur votre contenu. L'absence de bannières publicitaires et d'options superflues facilite la navigation. L'arborescence est peu profonde : toutes les fonctionnalités sont accessibles en moins de trois clics. Les pictogrammes guident l'utilisateur sur chacune des rubriques. Vous retrouverez, sur la page d'accueil d'un projet, une synthèse comprenant le calendrier d'avancement du projet, le formulaire de saisie d'activité et la liste des dernières actions concernant le projet. </p>
      <p>L'onglet jalon, à droite, vous invite à définir les différentes étapes de votre projet. Un titre, une description et une date limite. Les jalons ont deux utilités dans l'application : ils permettent d'une part de mesurer l'état d'avancement du projet (le pourcentage disponible est calculé en fonction du nombre de jalons passés) et d'autre part, ils servent de points de repère pour organiser les tâches. Ces dernières sont organisées par listes, c'est-à-dire qu'un paquet de tâches est rattaché à un jalon. Chacune est composée d'un titre, d'une description et d'une date limite. La tâche appartient à une liste et peut évidemment être affectée à un utilisateur. On appréciera l'éditeur visuel qui permet très simplement de mettre en forme les descriptions de tâches.</p>
      <p>Vous avez délimité les contours de votre projet, il est maintenant temps d'en avertir les bénévoles et vos éventuels partenaires en leur envoyant un message qui peut être rattaché à un jalon et envoyé à un ou plusieurs utilisateurs. Vous avez par ailleurs la possibilité de rendre visible le document uniquement à certaines catégories d'utilisateurs.</p>
      <p>Enfin, les tâches et les documents peuvent être « étiquetés » : vous leur attribuez des mots-clefs qui permettent, par la suite, de les retrouver rapidement en tapant ce même mot dans la barre de recherche. Pour finir, un système de messagerie instantanée vous permet de dialoguer en direct avec les personnes connectées.</p>
    </section>
    <section epub:type="bodymatter subchapter">
      <h3 id="stockage">Stockage et partage dans les nuages</h3>

      <dl id="owncloud">
        <dt class="logiciel"><i lang="en" xml:lang="en">Owncloud</i></dt>
        <dd class="type">Hébergement personnel</dd>
        <dd>
          <ul>
            <li class="apprent">Apprentissage : <img src="../images/level/hirond1.png" alt="facile" /></li>
            <li class="install">Installation : <img src="../images/level/fleur2.png" alt="moyen" /></li>
          </ul>
        </dd>
      </dl>

      <p>Si vous êtes à la recherche d'un espace d'échanges et de mutualisation de données, <a href="http://owncloud.org/"><i lang="en" xml:lang="en">Owncloud</i></a> pourrait répondre à vos attentes. Échanger des données est l'activité principale de tout projet collectif. Pourtant, les courriels et les réseaux sociaux ne sont pas forcément les outils les plus pertinents. Permettre à vos utilisateurs de déposer ou de télécharger des éléments regroupés en un seul endroit diminue largement l'encombrement des courriels avec de multiples versions du même document ou, plus simplement, de ne pas permettre à tout le monde d'y avoir accès. Vous pouvez y déposer le dernier compte-rendu de l'Assemblée Générale, les fiches d'inscriptions des membres ou encore les photos de la sortie de fin d'année. L'une des conditions est de pouvoir le faire avec un système stable et sécurisé.</p>

      <figure>
        <img src="../images/screenshots/owncloud.png" alt="" class="screen" />
        <figcaption>Stockez et partagez avec <i lang="en" xml:lang="en">Owncloud</i></figcaption>
      </figure>

      <p>Se servir d'<i lang="en" xml:lang="en">Owncloud</i> implique de pouvoir l'installer vous-même sur un serveur ou d'utiliser un service tiers, basé sur ce même logiciel. Dans le second cas, la question qui se pose est de savoir quel degré de confiance vous pouvez avoir envers celui qui propose ce service. Certaines associations le proposent selon une charte de confiance crédible et loyale, c'est le cas par exemple de Framasoft avec son service <a href="https://framadrive.org/">Framadrive</a> ou de (<a href="https://www.zaclys.com/">La Mère Zaclys</a> qui propose un accès gratuit de base et un accès payant pour agrandir l'espace disponible. Dans tous les cas, méfiez-vous des services gratuits ou des « box » dont vous ne connaissez pas les détails techniques. En la matière, payer pour un service reposant sur un logiciel libre reconnu ou se contenter d'un accès gratuit mais éthique, restent encore des solutions conciliantes.</p>

      <div class="astuce">
        <p class="titre">Faites attention</p>
        <p>La sécurité du Cloud Computing est aujourd'hui un secteur d'activité en plein essor car il représente un enjeu stratégique majeur et pas seulement pour les États et les grandes entreprises. Si vous installez un tel service pour vous-mêmes, n'oubliez pas d'inclure dans vos projections le temps-homme nécessaire à l'entretien et la sécurité des données, au delà des simples mises à jour du logiciel. Cela implique aussi de faire respecter quelques règles basiques de sécurité par vos utilisateurs.</p>
      </div>

      <p><i lang="en" xml:lang="en">Owncloud</i> offre aussi, par défaut (d'autres extensions sont disponibles), la possibilité de synchroniser ses contacts son agenda et ses tâches. Pour toutes ces fonctions, des applications pour smartphone et pour votre ordinateur de bureau sont disponibles, ce qui évite de se connecter sur une interface web pour faire les opérations demandées.</p>
      <p>Si <i lang="en" xml:lang="en">Owncloud</i> est un des plus célèbres systèmes de stockage et de partage de données, d'autres solutions méritent d'être testées. En voici trois qui tiennent la comparaison avec <i lang="en" xml:lang="en">Owncloud</i> : <a href="https://pyd.io/">Pydio</a>, <a href="https://www.seafile.com">Seafile</a> et <a href="https://cozy.io/">Cozycloud</a>.</p>
    </section>
    <section epub:type="bodymatter subchapter">
      <h3 id="sondage">Réaliser un sondage, faire une enquête</h3>

      <p>De nombreux outils libres tels <a href="http://papilio.niadomo.net/">Papillon</a>, <a href="http://pollen.chorem.org/pollen/">Pollen</a>, <a href="http://www.framadate.org/">Framadate</a> ou <a href="http://www.poll-o.fr">Poll-O</a> existent désormais pour permettre à des adhérents d'exprimer un choix. À partir d'un certain nombre de personnes à sonder, consulter chacun individuellement prend du temps et rend difficile une convergence. Avec ces outils, vos interlocuteurs peuvent faire part de leurs choix au moment où ils le souhaitent et la synthèse apparaît en temps réel.</p>

      <figure>
        <img src="../images/screenshots/framadate.png" alt="" class="screen" />
        <figcaption>Organisez des rendez-vous avec Framadate</figcaption>
      </figure>

      <p>Lorsqu'il s'agit de mener une enquête approfondie avec un questionnaire modulable, vous pouvez choisir d'utiliser <a href="http://www.limesurvey.org"><i lang="en" xml:lang="en">Limesurvey</i></a>. L'interface est plus compliquée à appréhender mais tous les types de questions peuvent être imaginés (cases à cocher, tri de termes par préférence, champ ouvert, questions conditionnelles, <abbr>etc.</abbr>). L'interface de gestion, accessible via un navigateur web, permet aussi d'accéder aux premières analyses quantitatives. Pour les personnes sondées, il est possible d'interrompre le questionnaire et de le reprendre plus tard. C'est sans aucun doute un logiciel intéressant pour les associations fonctionnant en réseau, ou pour les fédérations d'associations, car elles pourront interpeller leurs membres aux quatre coins du territoire. </p>
      <p>Plusieurs solutions s'offriront à vous quand viendra l'heure de choisir la meilleure manière de mettre en ligne le questionnaire. Soit votre besoin est ponctuel et le sondage n'est pas un « enjeu stratégique » pour votre association. Vous pouvez opter pour un service en ligne directement sur le site du projet. Soit votre objectif est de gérer de nombreux sondages et vous souhaitez garder la maîtrise de vos données : vous pouvez alors choisir d'installer sur votre propre serveur la dernière version du logiciel.</p>
    </section>
    <section epub:type="bodymatter subchapter">
      <h3 id="calendrier">Partager un calendrier</h3>

      <p>Le besoin est régulièrement exprimé par les associations de partager un calendrier, que ce soit pour permettre la planification du partage de salles et des outils, ou pour visualiser les emplois du temps de plusieurs personnes de l'association.</p>
      <p>Avant d'opter pour un logiciel, n'hésitez pas à regarder si les outils déjà en place dans l'association n'offrent pas cette possibilité par défaut, ou moyennant l'installation d'un petit programme complémentaire : vous l'avez vu à la lecture du paragraphe destiné aux SGC, ces types de modules sont généralement prévus.</p>
      <p>Si toutefois vous souhaitez un outil dédié, ou si vous rencontrez des soucis de compatibilité de format de données entre vos appareils mobiles et votre gestionnaire d'agenda, vous pouvez vous tourner vers <a href="http://www.phenix.gapi.fr/"><i lang="en" xml:lang="en">Phenix Agenda</i></a>.</p>

      <p>Enfin, tous les logiciels précédemment cités dans la partie « <i lang="en" xml:lang="en">cloud computing</i> » tels <i lang="en" xml:lang="en">Owncloud</i>, Pydio, <i lang="en" xml:lang="en">Seafile</i> et <i lang="en" xml:lang="en">Cozycloud</i>,  permettent la gestion d'agendas multiples et leur partage.</p>
    </section>
    <aside epub:type="footnotes" class="aside">
      <ul>
        <li> <a epub:type="footnote" href="#ref_1" id="note_1">[1]</a> Le groupe d'utilisateurs de logiciels libres de Paris et Île-de-France.</li>
      </ul>
    </aside>

  </body>
</html>
