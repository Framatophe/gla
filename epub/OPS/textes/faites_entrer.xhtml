<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="fr" lang="fr">
  <head>
    <title># Faites entrer les logiciels libres</title>
    <link rel="stylesheet" href="../styles/librasso.css" type="text/css" />
  </head>

  <body epub:type="bodymatter chapter">
    <h1 id="fiche_logiciel" class="titre-chapitre"># Faites entrer les logiciels libres</h1>
    <p class="epigraph" epub:type="bodymatter epigraph">
    <img src="../images/bonhommes/ptibonhomme3a.png" alt="" class="bonhomme" /></p>
    <p>  Abolir les frontières<br />
      entre concepteurs<br />
      et utilisateurs
    </p>

    <p><span class="intro">Le logiciel libre, plus qu'un outil : un enjeu pour votre association ! —</span> Le mouvement du logiciel libre est sans conteste le plus vaste mouvement d'émancipation et de partage de la connaissance qui se soit développé via Internet. Il réunit à ce jour des centaines de millions de contributeurs du monde entier. Par définition, il partage de nombreuses valeurs avec le monde associatif et plus particulièrement avec l'éducation populaire.</p>

    <p>Les logiciels libres se sont développés via Internet mais ils ont également rendu possible ce « réseau des réseaux ». La majeure partie de l'infrastructure d'Internet fonctionne grâce à du logiciel libre : à chaque fois que vous utilisez Internet, des dizaines, voire des centaines de logiciels libres s'activent pour vous rendre service, à commencer par le cœur de la « box » de votre fournisseur d'accès. Il existe également des logiciels libres pour écouter de la musique, rédiger des documents, jouer, apprendre en s'amusant, etc. Et il y a forcément un logiciel libre prêt à répondre aux besoins de votre association, par exemple en matière de gestion ou de travail collaboratif.</p>

    <h2 id="definition" class="titre-chapitre">Définition</h2>

    <p>Un logiciel libre est avant tout un logiciel respectant les libertés de ses utilisateurs. Plus précisément, la Fondation pour le Logiciel Libre (la <a href="http://www.fsf.org">Free Software Foundation</a>) définit le logiciel libre par les <em>quatre libertés</em> suivantes :</p>
      <ul>
        <li>la liberté d'exécuter le programme, pour tous les usages ;</li>
        <li>la liberté d'étudier le fonctionnement du programme et de l'adapter aux besoins ;</li>
        <li>la liberté de distribuer des copies du programme ;</li>
        <li>la liberté d'améliorer le programme et d'en diffuser les améliorations.</li>
      </ul>

    <p>Ces quatre libertés sont fondamentales et changent l'informatique dans sa relation aux usagers tout en créant de nouveaux équilibres organisationnels et économiques. Ces libertés contribuent à enrichir chaque jour un « Commun » que personne ne peut se réapproprier au détriment des autres.</p>

    <figure>
      <img src="../images/illustrations/Poster_LL_comment_ca_marche.png" alt="" class="illustration" />
      <figcaption><a href="http://www.april.org/sensibilisation"> Le poster « Le Logiciel Libre : comment ça marche ? » sur le site de l’April.</a></figcaption>
    </figure>
    <p>Les seconde et quatrième libertés vous offrent la possibilité d'adapter le logiciel ou de le faire faire par une tierce personne qui possède les connaissances nécessaires. Ces libertés illustrent le partage du savoir et répondent à une logique évidente de ne pas systématiquement devoir ré-inventer des outils qui existent déjà. On profite du logiciel existant et, en retour, on peut faire profiter la communauté de nouvelles améliorations, simplement en les partageant.</p>

    <div class="astuce">
      <p class="titre">Pour aller plus loin</p>
      <p>Pour comprendre l'histoire du mouvement du logiciel libre, vous pouvez commander l'ouvrage biographique <cite>Richard Stallman et la révolution du logiciel libre</cite> (Paris : Framasoft-Eyrolles, 2010) ou le consulter sur le <a href="http://framabook.org">site du projet Framabook</a>.</p>
    </div>

    <p>Enfin, la troisième liberté vous permet de partager des copies du logiciel avec qui vous voulez. Par exemple, une personne venue se former sur un logiciel libre peut repartir avec l'outil sur lequel elle a travaillé, une fois l'atelier terminé. Un bénévole peut utiliser les mêmes logiciels libres, chez lui ou au sein de son association.</p>

    <p><span class="intro">Pourquoi choisir des logiciels libres pour votre association ? —</span> Même si votre association n'utilise pas toutes ces libertés au quotidien (par exemple la liberté de modifier le programme), le choix des logiciels libres n'est pas neutre. Dans la mesure où nous exerçons de plus en plus nos actions quotidiennes par l'intermédiaire de nos ordinateurs, la liberté logicielle s'impose comme un prérequis nécessaire pour pouvoir exercer les libertés les plus fondamentales. Un professeur de droit américain de renom, Lawrence Lessig, a d'ailleurs illustré ce fait avec l'expression « <i lang="en" xml:lang="en">Code is Law</i> », c'est-à-dire « Le code logiciel est la loi <a href="http://www.framablog.org/index.php/post/2010/05/22/code-is-law-lessig">informatique</a> ». Bref, le logiciel libre est une brique nécessaire (mais non suffisante) pour permettre de garantir la vie privée des utilisateurs.</p>


    <div class="astuce">
      <p class="titre">Note importante</p>

      <p>Il existe plusieurs <em>licences libres</em> obéissant aux principes des 4 libertés informatiques et qui déclinent les conditions de leur exercice. Vous pouvez lire à ce propos la <a href ="https://fr.wikipedia.org/wiki/Licence_libre">page explicative de Wikipédia</a>.</p>
    </div>

    <p>Ceci nous permet de mieux comprendre les enjeux portés par le libre et le parallèle avec la rédaction d'une loi est tout à fait pertinent : écrire et comprendre les textes de loi est une entreprise complexe et technique. Pour autant, le fait que les textes produits soient soumis au contrôle et à la bienveillance des citoyens représente un enjeu majeur de démocratie.</p>

    <p>Le choix de logiciels libres répond donc également au projet associatif comme outil pédagogique et de sensibilisation sur la question de l'appropriation technologique. Il constitue de surcroît une offre mature pour soutenir votre projet associatif au quotidien.</p>

    <p>En résumé, voici quelques-uns des multiples avantages qu'offrent les logiciels libres aux associations :</p>
      <ul>
        <li>éthique du partage de la connaissance ;</li>
        <li>réduction du coût d'accès à l'informatique ;</li>
        <li>copie en toute légalité des logiciels et donc solution naturelle et évidente à la contrefaçon<a epub:type="noteref" href="#note_1" id="ref_1"> [1]</a> ;</li>
        <li>adaptation de logiciels aux besoins, en toute indépendance ;</li>
        <li>fiabilité, interopérabilité et respect des standards ;</li>
        <li>insensibilité aux virus Microsoft Windows ;</li>
        <li>pérennité et évolution du système d'information ;</li>
        <li>nouvelles versions avec de nouvelles fonctionnalités, disponibles régulièrement ;</li>
        <li>seconde vie de vieux parcs informatiques ;</li>
        <li>protection de la vie privée et contrôle de vos machines ;</li>
      </ul>

    <p class="resume">Utiliser de l'informatique libre, ce n'est pas seulement faire des économies, c'est instaurer de nouveaux équilibres.</p>

    <h2 class="titre-chapitre">Les associations et leurs équipements informatiques en France</h2>

    <p>Avant de parler de l'usage de l'informatique dans les associations, il convient de décrire succinctement le paysage associatif français ; ceci afin de prendre conscience du nombre de structures touchées et de leur diversité. En France, plus d'1,3 million d'associations sont actives, maillent le territoire et assurent l'emploi de plus d'1,8 millions de personnes à temps partiel ou à temps plein. Nous vous invitons à parcourir la synthèse intitulée « <a href="http://cpca.asso.fr/wp-content/uploads/2012/05/les-associations-en-france-aujourdhui.pdf">Repères 2012 sur les associations en France</a> » issue des travaux du <abbr>CNRS</abbr> par Edith Archambault et Viviane Tchernonog.</p>

    <p>Ce qui caractérise le milieu associatif est évidemment l'implication de bénévoles dans la majeure partie d'entre elles : 85% des associations françaises fonctionnent sans salariés.</p>

    <p>Dans sa dernière étude, <cite>Le Paysage associatif français</cite> (2013), Viviane Tchernonog indique que l'augmentation du nombre d'associations de 2,8%, entre 2005 et 2012, a principalement été le fait de petites associations et plus généralement des associations de bénévoles. Elle précise que 22% des associations gèrent un budget annuel inférieur à 1 000 euros et 49%, un budget compris entre 1 000 et 10 000 euros. Fin 2013, 1 652 responsables d'associations ont été interrogés par <a href="http://recherches-solidarites.org/">Recherches &amp; Solidarités</a>, sur leurs usages et leurs perceptions du numérique. Parmi les difficultés rencontrées dans la mise en place et l'utilisation des outils numériques, ils sont 54% à citer le manque de temps, 43%, le manque de savoir-faire et 38% le manque de moyens financiers et matériels.</p>

    <p>Enfin, pour une très grande majorité les associations n'ont pas de locaux pour exercer leurs activités. On peut donc supposer que les adhérents des associations effectuent un certain nombre de tâches depuis leur domicile. Les pratiques informatiques associatives sont ainsi très liées aux pratiques informatiques des particuliers.</p>

    <p class="resume">Ces quelques éléments permettent de préciser le périmètre et le profil des associations d'abord visées par ce guide.</p>

    <div class="astuce">
      <p class="titre">Remarque</p>
      <p>Le groupe de travail Libre Association de l'April a mené deux enquêtes (<a href="http://libreassociation.info/article34.html">une première</a> en 2009 et <a href="http://libreassociation.info/article36.html">une seconde</a> en 2015) auprès d'associations pour connaître leurs rapports aux logiciels libres. <em>Une conclusion de la comparaison de ces enquêtes est que la question principale en 2009 qui était « qu'est-ce que sont les logiciels libres ? » est devenue en 2015 « Comment fait-on pour les adopter ? »</em>.</p>
    </div>

    <p> <span class="intro">Équipement en matériel —</span> La taille du parc informatique des associations est généralement faible, comme le montre <a href="http://outils-reseaux.org/SyntheseQuestionnaire">l'enquête réalisée par Outils-Réseaux</a>. Plus des deux tiers des associations possèdent entre 1 et 5 ordinateurs.  L'informatique est perçue comme un outil indispensable puisque très peu d'associations ne sont pas du tout équipées mais les moyens disponibles ne permettent généralement pas de dédier du temps et des compétences professionnelles pour son développement et son entretien.</p>

    <p><span class="intro">Systèmes d'exploitation utilisés —</span> Le système d'exploitation le plus utilisé par les associations est évidemment Windows de Microsoft, depuis longtemps en situation d'abus de position dominante et appliquant la vente forcée en accord avec les constructeurs. Mais la situation semble évoluer favorablement vers une adoption plus importante de systèmes d'exploitation libres : les chiffres fournis par la dernière enquête sont d'ailleurs étonnamment hauts en ce qui concerne la question « L'association utilise-t-elle des systèmes d'exploitation libres pour les postes de travail tels GNU/Linux (par exemple Debian, Trisquel, Ubuntu, Linux Mint, etc.), *<abbr>BSD</abbr> ? »  car 42% des 231 personnes interrogées répondent positivement à cette question. Puisque personne n'a réellement d'intérêt (ou les personnes qui auraient un intérêt n'ont pas les moyens de mesurer à grande échelle) à évaluer la part des systèmes d'exploitation libres utilisés, il est probable que les chiffres couramment admis sont erronés.</p>

    <p>Par ailleurs, l'enquête réalisée par Outils-Réseaux montre dans le même temps que le budget pour les licences d'utilisation des systèmes d'exploitation et des logiciels est inexistant dans plus de 60% des cas. Cela peut être interprété de plusieurs façons : d'une part les associations n'ont peut-être pas conscience, lors de l'achat d'équipement informatique, que le prix des licences est inclus dans la facture. D'autre part, des associations utilisent sans doute des logiciels privateurs commerciaux via l'équipement personnel de leurs membres.</p>


    <p> <span class="intro">Autour des ressources humaines —</span> Généralement, ce sont des bénévoles qui assument la gestion de l'informatique (voir l'enquête de <a href="http://www.april.org/files/analyse-questionnaire-associations-informatique-et-logiciels-libres.pdf">Libre Association</a>, ce qui conduit à des situations complexes : temps de présence physique réduit dans les locaux (quand il y en a), soutien distant ou tentative d'auto-régulation par l'usager confronté à une situation de blocage. Or, l'informatique est souvent utilisée pour des tâches essentielles à l'association : gestion des adhérents, comptabilité-gestion, tâches administratives avec un logiciel dédié, etc.</p>

    <p>Malheureusement, cette prise de conscience de l'importance de l'informatique et des compétences spécifiques nécessaires se fait parfois dans des situations de pertes de données ou de sauvegardes défaillantes (quand il y en a).</p>

    <p class="resume">Ce guide et les conseils qui y sont prodigués ont aussi pour objet de contribuer modestement à réduire ces situations de crise.</p>

    <p><span class="intro">Communication interne et externe —</span> Les sites web « institutionnels » sont désormais courants pour les associations. Toutefois, seule une relative minorité d'entre elles déclare utiliser leur site web dans un but collaboratif. Une portion plus restreinte encore utilise le site web comme un outil pour échanger avec l'extérieur. Certes les réseaux sociaux bousculent ces pratiques, mais il y a de fait un enjeu à ce que les associations s'approprient mieux ces nouvelles possibilités de collaborer.</p>

    <div class="astuce">
      <p class="titre">S’entourer</p>
      <p>Les « communautés » de logiciels libres regroupent des utilisateurs et des collaborateurs qui utilisent et produisent ces logiciels. Si vous utilisez ou contribuez, vous ferez partie d'au moins une de ces communautés. Mais cela ne suffit pas toujours : pour intégrer un logiciel libre dans votre organisation, vous pourriez avoir besoin de formations, de développer des fonctionnalités spécifiques ou d'assurer un support technique plus ou moins important selon la taille de votre organisation. Pour cela, vous pouvez contacter des sociétés qui proposent des services en logiciels libres. Le réseau <a href="http://www.libre-entreprise.org/">Libre-Entreprise</a> et le <a href="http://cnll.fr/">Conseil National du Logiciel Libre</a> sont deux portes d'entrée intéressantes pour cela.</p>
    </div>

    <img src="../images/illustrations/catalogue_libre_1erecouv.png" alt="" />

    <p>L'April a publié fin 2010 un <a href="http://www.april.org/Catalogue\_Libre">catalogue de logiciels libres</a> intitulé <cite>Catalogue Libre. 26 logiciels libres à découvrir</cite>, rédigé par le groupe de travail Sensibilisation.</p>

    <p>Agrémenté d'autres informations sur les enjeux du logiciel libre, ce catalogue constitue une première sélection de logiciels grand public, sélectionnés pour leurs qualités et organisés par domaine d'application (Multimédia, Bureautique, Éducation, Internet) pour permettre à ceux qui découvrent les logiciels libres de ne pas être perdus dans le large choix des applications disponibles.</p>

    <aside epub:type="footnotes" class="aside">
      <p> <a epub:type="footnote" href="#ref_1" id="note_1">[1]</a> Souvent désignée de manière impropre sous le terme « <a href="http://www.gnu.org/philosophy/words-to-avoid.fr.html#Piracy">piratage</a> ».</p>
    </aside>
  </body>

</html>
